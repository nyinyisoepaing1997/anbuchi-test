
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="doc/devops.png" alt="Logo" width="180" height="80">
  </a>

  <h3 align="center">Test Assignment for Senior Cloud Engineer</h3>

</div>

## Table of Contents

1. [Introduction](#introduction)

2. [PwnedLabs](PwnedLabs)

   - 2.1  [Pillage Exposed RDS Instances](#pillage-exposed-rds-instances)

   - 2.2  [Loot Public EBS Snapshots](#Loot-public-ebs-snapshots)

   - 2.3  [Identify the AWS Account ID from a Public S3 Bucket](#identify-the-aws-account-id-from-a-public-s3-bucket)

   - 2.4  [AWS S3 Enumeration Basics](#aws-s3-enumeration-basics)

   - 2.5  [Path Traversal to AWS credentials to S3](#path-traversal-to-aws-credentials-to-s3)
   
3. [Conclusion](#conclusion)


### Introduction

I discovered numerous cloud-based labs on Pwnlabs. Personally, I possess extensive experience and confidence working with AWS. Consequently, I successfully completed 5 labs related to AWS.

### PwnedLabs
#### Pillage Exposed RDS Instances

[description and screenshot](./2.1.md)

### Loot Public EBS Snapshots
[description and screenshot](./2.2.md)

### Identify the AWS Account ID from a Public S3 Bucket
[description and screenshot ](./2.3.md)

### AWS S3 Enumeration Basics
[description and screenshot](./2.4.md)

### Path Traversal to AWS credentials to S3
[description and screenshot](./2.5.md)

### Conclusion

I have extensive experience in cloud security, particularly in dealing with misconfigurations in IAM (Identity and Access Management) and cloud engineering labs.